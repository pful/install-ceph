variable "SSH_USER" {
  default = "pful"
}

variable "SSH_KEY" {}

// Configure the Google Cloud provider
provider "google" {
  credentials = "${file("account.json")}"
  project     = "celestial-gist-209305"
  region      = "us-west1"
}

// Create a new instance
resource "google_compute_instance" "default" {
  name         = "ceph-admin-node"
  machine_type = "custom-1-2048"
  zone         = "us-west1-b"

  metadata {
    sshKeys = "${var.SSH_USER}:${file("${var.SSH_KEY}/id_rsa.pub")}"
  }

  boot_disk {
    initialize_params {
      image = "keystone-base"
    }
  }

  network_interface {
    network = "default"
    address = "10.138.0.50"

    access_config {
      // Ephemeral IP
    }
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  connection {
    type        = "ssh"
    user        = "pful"
    agent       = "false"
    private_key = "${file("${var.SSH_KEY}/id_rsa")}"
  }

  provisioner "remote-exec" {
    inline = [
      "apt update",
      "apt install -t vim net-tools git",
      "cd /${var.SSH_USER}",
      "git clone https://daeyeon@bitbucket.org/pful/install-ceph.git",
      "cd install-ceph",
    ]
  }
}
