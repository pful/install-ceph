#!/bin/bash

./setup-node.sh

ssh-gen
ssh-copy-id pful@monitor-node
ssh-copy-id pful@manager-node
ssh-copy-id pful@osd-node
ssh-copy-id pful@mds-node