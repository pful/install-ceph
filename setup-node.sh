#!/bin/bash

apt install -y ntp openssh-server sudo

useradd -d /home/pful -m pful
passwd pful

echo "pful ALL = (root) NOPASSWD:ALL" | .
tee /etc/sudoers.d/pful
chmod 0440 /etc/sudoers.d/pful

